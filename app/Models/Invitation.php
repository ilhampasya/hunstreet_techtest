<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invitation extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'status_id',
        'user_id',
        'registration_code',
        'expired_at',
        'status_id',
        'completed_at'
    ];

    protected $dates = [
        'expired_at',
        'completed_at'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function isValid()
    {
        return $this->status->name === 'pending' && now()->timestamp < $this->expired_at->timestamp;
    }
}
