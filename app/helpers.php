<?php

if (!function_exists('get_routes')) {
    function get_routes() {
        return collect(app('router')->getRoutes())->map(function ($route) {
            return [
                'name' => $route->getName(),
                'path' => sprintf('/%s', trim($route->uri))
            ];
        })->flatMap(function ($route) {
            return [$route];
        });
    }
}