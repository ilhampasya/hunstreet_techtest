<?php

namespace App\Http\Controllers;

use App\Http\Requests\CompleteRequest;
use App\Mail\CompletedInvitationMail;
use App\Models\Status;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        return view('index', [
            'user' => $request->user()
        ]);
    }

    public function complete(CompleteRequest $request)
    {
        $user = $request->user();

        DB::beginTransaction();

        try {
            $user->update([
                'name' => $request->name,
                'date_of_birth' => $request->date_of_birth,
                'gender' => $request->gender,
            ]);

            $user->designers()->sync($request->designers);

            $user->invitation->update([
                'status_id' => Status::where('name', 'success')->first()->id,
                'registration_code' => strtoupper(Str::random(8)),
                'completed_at' => ($now = now()),
            ]);

            Mail::later($now->addHours(1), new CompletedInvitationMail($user->invitation));

            DB::commit();
        } catch (\Throwable $e) {
            DB::rollback();
            
            return redirect()->back()->withError('Something went wrong.');
        }

        return redirect()->back()->withSuccess('Registration succesfully.');
    }
}
