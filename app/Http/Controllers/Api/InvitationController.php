<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Invitation;
use Illuminate\Http\Request;

class InvitationController extends Controller
{
    public function index(Request $request)
    {
        $search = json_decode($request->search ?: '{}', true);

        $query = Invitation::query()->with(['user', 'status']);

        return response()->json([
            'success' => true,
            'code' => 200,
            'data' => $query->orderBy('id', 'desc')->paginate($request->perPage ?: 10)
        ]);
    }
}
