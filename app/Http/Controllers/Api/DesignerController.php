<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Designer;
use Illuminate\Http\Request;

class DesignerController extends Controller
{
    public function index(Request $request)
    {
        $search = json_decode($request->search ?: '{}', true);

        $query = Designer::query();
        
        foreach ($search as $key => $value) {
            $query = $query->where($key, 'like', sprintf("%%%s%%", $value));
        }

        return response()->json([
            'success' => true,
            'code' => 200,
            'data' => $query->orderBy('name', 'asc')->paginate($request->perPage ?: 10)
        ]);
    }
}
