<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class VerifyInvitation
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (!$this->isValidRequest($request)) {
            abort(400);
        }

        return $next($request);
    }

    private function isValidRequest(Request $request)
    {
        try {
            if (($user = User::with(['invitation', 'invitation.status'])->where('email', $email = Crypt::decrypt($request->signature))->first())) {
                $request->setUserResolver(function () use ($user) {
                    return $user;
                });
                return true;
            }

            return false;
        } catch (DecryptException $e) {
            return false;
        }
    }
}
