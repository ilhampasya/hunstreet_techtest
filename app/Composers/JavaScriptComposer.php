<?php

namespace App\Composers;

use JavaScript;
use Illuminate\Contracts\View\View;

class JavaScriptComposer
{
    public function compose(View $view)
    {
        JavaScript::put([
            'route' => request()->route() ? [
                'name' => request()->route()->getName(),
                'path' => request()->route()->uri
            ] : null,
            'routes' => get_routes(),
            'baseUrl' => url('/'),
            'user' => tap(request()->user(), function ($user) {
                if ($user) {
                    $user->load(['role']);
                }
            }),
            'csrfToken' => csrf_token()
        ]);
    }
}