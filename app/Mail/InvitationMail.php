<?php

namespace App\Mail;

use App\Models\Invitation;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class InvitationMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    private $invitation;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Invitation $invitation)
    {
        $this->queue = 'invitation:link';
        
        $this->invitation = tap($invitation, function ($invitation) {
            $invitation->load(['user', 'user.role']);
        });
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->to($this->invitation->user->email)
            ->subject('Invitation Link')
            ->view('mails.invitation-link', [
                'invitation' => $this->invitation
            ]);
    }
}
