<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('/v1')->name('v1.')->group(function () {
    Route::prefix('/designers')->name('designers.')->group(function () {
        Route::get('/', 'DesignerController@index')->name('index');
    });
    Route::prefix('/invitations')->name('invitations.')->group(function () {
        Route::get('/', 'InvitationController@index')->name('index');
    });
});
