<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('verify.invitations')->group(function () {
    Route::get('/', 'HomeController@index')->name('index');
    Route::middleware('verify.expiration.invitations')->post('/', 'HomeController@complete')->name('complete');
});

Route::prefix('/dashboard')->middleware('auth')->namespace('Dashboard')->name('dashboard.')->group(function () {
    Route::get('/', 'HomeController@index')->name('index');
    Route::prefix('/invitations')->name('invitations.')->group(function () {
        Route::get('/', 'InvitationController@index')->name('index');
        Route::post('/generate', 'InvitationController@generate')->name('generate');
    });
});
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
