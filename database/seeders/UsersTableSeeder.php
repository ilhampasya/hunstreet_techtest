<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
        User::create([
            'name' => 'Admin',
            'email' => 'admin@hunstreet.com',
            'password' => Hash::make('password'),
            'role_id' => Role::where('name', 'admin')->first()->id
        ]);
    }
}
