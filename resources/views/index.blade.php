@extends('layouts.app')
@section('app')
<div class="min-h-screen bg-gray-100 flex justify-center px-7 lg:px-0" style="background-image: -webkit-linear-gradient(90deg, #f3f4f6 55%, #437cc1 45%)">
    <div class="mx-auto w-full lg:w-1/2 py-12 lg:py-14">
        <card>
            <template v-slot:header>
                <div class="flex flex-col items-center justify-center">
                    <div>
                        <span class="font-semibold text-3xl text-blue-400 my-auto">TechTest</span>
                    </div>
                </div>
            </template>
            <template>
                @if ($user->invitation->isValid())
                <form method="POST" action="{{ route('complete', ['signature' => request()->signature]) }}" class="flex flex-col gap-5 lg:gap-6">
                    @csrf
                    <div class="grid grid-cols-1 lg:grid-cols-2 gap-y-5 lg:gap-y-6 gap-x-3.5">
                        <text-input :errors='@json($errors->get("email"))' :input-props="{ name: 'email', placeholder: 'E-mail', autocomplete: 'off', disabled: true }" value="{{ old('email', $user->email) }}" placeholder="E-mail"></text-input>
                        <text-input :errors='@json($errors->get("name"))' :input-props="{ name: 'name', placeholder: 'Name', autocomplete: 'off' }" value="{{ old('name') }}" placeholder="Name"></text-input>
                    </div>
                    <div class="grid grid-cols-1 lg:grid-cols-2 gap-y-5 lg:gap-y-6 gap-x-3.5">
                        <date-of-birth-input :errors='@json($errors->get("date_of_birth"))' :value="'{{ old('date_of_birth') }}'" placeholder="Date Of Birth"></date-of-birth-input>
                        <gender-slct :errors='@json($errors->get("gender"))' :value="'{{ old('gender') }}'">
                        </gender-slct>
                    </div>
                    <div>
                        <designers-slct :errors='@json($errors->get("designers"))' :options='{{ old("designers_initials", "{}") }}' :value='@json(old("designers", []))'>
                        </designers-slct>
                    </div>
                    <div class="flex justify-between border-t-2 -mx-6 -mb-3 pt-1.5 pb-0.5 mt-5 px-6 items-center">
                        <div class="flex items-center gap-3 text-gray-500">
                            <div class="flex gap-3.5" countdown>
                                <div class="flex flex-col justify-center items-center" countdown-type="days">
                                    <span class="font-semibold">0</span>
                                    <span class="font-medium text-xs -mt-1">days</span>
                                </div>
                                <div class="flex flex-col justify-center items-center" countdown-type="hours">
                                    <span class="font-semibold">0</span>
                                    <span class="font-medium text-xs -mt-1">hours</span>
                                </div>
                                <div class="flex flex-col justify-center items-center" countdown-type="minutes">
                                    <span class="font-semibold">0</span>
                                    <span class="font-medium text-xs -mt-1">minutes</span>
                                </div>
                                <div class="flex flex-col justify-center items-center" countdown-type="seconds">
                                    <span class="font-semibold">0</span>
                                    <span class="font-medium text-xs -mt-1">seconds</span>
                                </div>
                            </div>
                        </div>
                        <div class="flex justify-end">
                            <btn type="submit" class="bg-white text-blue-500 hover:bg-blue-600 hover:text-white flex items-center gap-2">
                                <svg viewBox="0 0 24 24" width="20" height="20" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1">
                                    <line x1="22" y1="2" x2="11" y2="13"></line>
                                    <polygon points="22 2 15 22 11 13 2 9 22 2"></polygon>
                                </svg>
                                <span>Submit</span>
                            </btn>
                        </div>
                    </div>
                </form>
                @else
                <div class="flex justify-center flex-col items-center">
                    <div class="font-medium uppercase text-sm">
                        <span>Your Registration Code</span>
                    </div>
                    <h3 class="font-semibold text-4xl text-gray-700 mt-5 ">{{ $user->invitation->registration_code }}</h3>
                    <div class="font-medium uppercase text-xs mt-5">
                        <span>Completed At {{ $user->invitation->completed_at->format('Y-m-d H:i:s') }}</span>
                    </div>

                </div>
                @endif
            </template>
            @if (!$user->invitation->isValid())
            <template v-slot:footer>
                <div class="flex">
                    <div class="italic uppercase text-sm">
                        <span><span class="text-xs text-gray-700 mt-5 ">*Please save this registration code as proof that you have completed the registration of the invitation that has been sent by us.</span></span>
                    </div>
                </div>
            </template>
            @endif
        </card>
    </div>
</div>
@stop
@push('scripts')
<script>
    const $el = $('[countdown]');
    const countDownDate = new Date(`{{ $user->invitation->expired_at->format('Y-m-d H:i:s') }}`).getTime();

    const updateTime = (value, type) => {
        if (value.toString().length === 1) {
            value = `0${value}`.substr(-2);
        }
        $($el.find(`[countdown-type="${type}"]`)[0].children[0]).html(value)
    };

    const x = setInterval(function() {
        const now = new Date().getTime();
        const distance = countDownDate - now;

        const times = {
            days: Math.floor(distance / (1000 * 60 * 60 * 24)),
            hours: Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)),
            minutes: Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60)),
            seconds: Math.floor((distance % (1000 * 60)) / 1000),
        };

        for (const type of ['days', 'hours', 'minutes', 'seconds']) {
            updateTime(times[type], type);
        }

        if (distance < 0) {
            clearInterval(x);
            window.location.reload();
        }
    }, 1000);
</script>
@endpush