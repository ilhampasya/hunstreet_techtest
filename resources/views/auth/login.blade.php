@extends('layouts.app', ['pageTitle' => 'Login - TechTest'])

@section('app')
<div class="flex justify-center min-h-screen bg-gray-100">
    <card class="m-auto">
        <form class="flex flex-col gap-4" method="POST" action="{{ route('login') }}">
            @csrf

            <div>
                <div>
                    <text-input :errors='@json($errors->get("email"))' :input-props="{ name: 'email', placeholder: 'E-mail', autocomplete: 'off' }" value="{{ old('email') }}" placeholder="E-mail" />
                </div>
            </div>

            <div>
                <div>
                    <text-input :errors='@json($errors->get("password"))' :input-props="{ name: 'password', placeholder: 'Password', autocomplete: 'off', type: 'password' }" value="{{ old('password') }}" placeholder="Password" />
                </div>
            </div>

            <div>
                <div>
                    <div>
                        <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                        <label class="form-check-label" for="remember">
                            {{ __('Remember Me') }}
                        </label>
                    </div>
                </div>
            </div>

            <div>
                <btn type="submit" class="bg-blue-500 text-white w-full">
                    {{ __('Login') }}
                </btn>
            </div>
        </form>
    </card>
</div>
@endsection