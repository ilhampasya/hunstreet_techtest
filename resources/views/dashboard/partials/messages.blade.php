<div class="fixed bottom-5 left-5 lg:left-10 w-full md:w-1/3 lg:1/5 z-50 flex flex-col gap-3">
    @foreach(['success', 'error'] as $type)
        @if (($message = session($type)))
            <flash-messages type="{{ $type }}" message="{{ $message }}"></flash-messages>  
        @endif
    @endforeach
</div>