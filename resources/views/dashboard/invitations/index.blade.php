@extends('layouts.dashboard', ['pageTitle' => 'Invitations - Dashboard'])
@section('main')
<div class="flex justify-between items-center">
    <div class="w-1/2">
        <dashboard-breadcrumbs :items="[
        {
            text: 'Home',
            is_active: true,
            to: 'dashboard.index',
        },
        {
            text: 'Invitations',
        }
    ]">

        </dashboard-breadcrumbs>
    </div>
    <div class="w-1/2 flex justify-end">
        <btn type="submit" class="bg-blue-500 text-white" @click="() => this.$refs.$modal.showModal()">Generate Link</btn>
    </div>
</div>
<card>
    <invitation-tbl />
</card>
<modal :initshow="{{ $errors->has('email') ? 'true' : 'false' }}" :with-padding-y="false" :style="{'--width': `${window.innerWidth > 600 ? window.innerWidth / 2.5 : window.innerWidth / 1.2}px`, margin: 'auto'}" ref="$modal">
    <template v-slot:header>
        <h3>Generate Link</h3>
    </template>
    <template>
        <form method="POST" class="py-5 flex flex-col gap-6" action="{{ route('dashboard.invitations.generate') }}">
            @csrf
            <div>
                <text-input :errors='@json($errors->get("email"))' :input-props="{ name: 'email', placeholder: 'E-mail', autocomplete: 'off' }" value="{{ old('email') }}" placeholder="E-mail"></text-input>
            </div>
            <div class="flex justify-end mt-4 gap-2.5">
                <btn type="button" class="bg-gray-200 text-gray-500" @click="() => this.$refs.$modal.showModal(false)">Close</btn>
                <btn type="submit" class="bg-blue-500 text-white">Generate</btn>
            </div>
        </form>
    </template>
</modal>
@stop