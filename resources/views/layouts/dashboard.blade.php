@extends('layouts.app', ['pageTitle' => $pageTitle ?? 'Dashboard'])
@section('app')
<div class="min-h-screen bg-gray-100 flex relative">
    <dashboard-sidebar ref="$sidebar"></dashboard-sidebar>
    <div class="flex-auto">
        <dashboard-header></dashboard-header>
        <main class="px-5 lg:px-10 py-6 relative min-h-screen">
            @yield('main')
        </main>  
    </div>
</div>
@stop