<div>
    Hello {{ $invitation->user->name ?? $invitation->user->role->display_name }},
    <br/>

    Here the invitation link for you:
    <a href="{{ $link = route('complete', ['signature' => Crypt::encrypt($invitation->user->email)]) }}">{{ $link }}</a>
</div>