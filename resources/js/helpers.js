export function route(name) {
    return window.routes.filter(({ name: routeName }) => routeName === name)[0]?.path;
}

export function objectLength(obj) {
    return Object.entries(obj).length;
}