/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

const { route } = require('./helpers');

require('./bootstrap');

window.Vue = require('vue').default;

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('btn', require('./components/Button.vue').default);

Vue.component('modal', require('./components/Modal.vue').default);

Vue.component('text-input', require('./components/TextInput.vue').default);

Vue.component('card', require('./components/Card.vue').default);

Vue.component('flash-messages', require('./components/FlashMessages.vue').default);

Vue.component('slct', require('./components/Select.vue').default);

Vue.component('slct-ajax', require('./components/SelectAjax.vue').default);

Vue.component('gender-slct', require('./components/GenderSelect.vue').default);

Vue.component('designers-slct', require('./components/DesignersSelect.vue').default);

Vue.component('date-input', require('./components/DateInput.vue').default);

Vue.component('date-of-birth-input', require('./components/DateOfBirthInput.vue').default);

Vue.component('loading-icon', require('./components/LoadingIcon.vue').default);

Vue.component('tbl', require('./components/Table.vue').default);

Vue.component('invitation-tbl', require('./components/dashboard/InvitationTable.vue').default);

Vue.component('dashboard-header', require('./components/dashboard/Header.vue').default);

Vue.component('dashboard-sidebar', require('./components/dashboard/Sidebar.vue').default);

Vue.component('dashboard-breadcrumbs', require('./components/dashboard/Breadcrumbs.vue').default);

Vue.component('dashboard-greetings', require('./components/dashboard/Greetings.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.mixin({
    data: () => ({
        window,
        currentRoute: window.route
    }),

    methods: {
        route,
    }
})

const app = new Vue({
    el: '#app',
});