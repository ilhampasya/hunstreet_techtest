# TechTest Documentation
## Installation
```sh
composer install
```
```sh
cp .env.example .env
```
#### Environtment Variables
```env
APP_URL=

QUEUE_CONNECTION=database

MAIL_MAILER=smtp
MAIL_HOST=
MAIL_PORT=2525
MAIL_USERNAME=
MAIL_PASSWORD=
MAIL_ENCRYPTION=tls
MAIL_FROM_ADDRESS="no-reply@hunstreet.net"
MAIL_FROM_NAME="${APP_NAME}"

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=hunstreet_techtest
DB_USERNAME=root
DB_PASSWORD=root
```
```sh
php artisan key:generate
```
```sh
php artisan migrate
```
```sh
php artisan db:seed
```
## Queue
- invitation:link
- invitation:completed

## Endpoints
- [base_url]?signature=[signature] -> register and show after registered
- [base_url]/dashboard -> admin
- [base_url]/login -> login
